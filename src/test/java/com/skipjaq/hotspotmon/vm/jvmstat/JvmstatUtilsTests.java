package com.skipjaq.hotspotmon.vm.jvmstat;

import com.skipjaq.hotspotmon.TestUtils;
import org.junit.Test;
import sun.jvmstat.monitor.MonitoredHost;
import sun.jvmstat.monitor.MonitoredVm;

import static junit.framework.TestCase.assertNotNull;

/**
 * @author Rob Harrop
 */
public class JvmstatUtilsTests {

    @Test
    public void testGetLocalMonitoredHost() {
        MonitoredHost localHost = JvmstatUtils.getLocalMonitoredHost();
        assertNotNull(localHost);
    }

    @Test
    public void getVm() {
        MonitoredVm monitoredVm = JvmstatUtils.getMonitoredVm(JvmstatUtils.getLocalMonitoredHost(), TestUtils.getPid());
        assertNotNull(monitoredVm);
    }

    @Test(expected = IllegalStateException.class)
    public void getInvalidVm() {
        JvmstatUtils.getMonitoredVm(JvmstatUtils.getLocalMonitoredHost(), Integer.MAX_VALUE);
    }
}
