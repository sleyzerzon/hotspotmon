package com.skipjaq.hotspotmon.vm.jvmstat;

import com.skipjaq.hotspotmon.TestApp;
import com.skipjaq.hotspotmon.vm.Vm;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;
import reactor.core.test.TestSubscriber;
import reactor.core.tuple.Tuple2;
import sun.jvmstat.monitor.MonitoredHost;
import sun.jvmstat.monitor.MonitoredVm;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Rob Harrop
 */
public class JvmstatVmTests {

    static final String TEST_PROPERTY = "com.skipjaq.test";

    static final String TEST_PROPERTY_VALUE = "test";

    private MonitoredHost host;

    private TestApp testApp;

    private Vm vm;

    @Before
    public void before() throws Exception {
        Properties properties = new Properties();
        properties.setProperty(TEST_PROPERTY, TEST_PROPERTY_VALUE);

        this.testApp = TestApp.spawn(properties);
        this.testApp.waitForStart();

        this.host = JvmstatUtils.getLocalMonitoredHost();
        MonitoredVm mvm = JvmstatUtils.getMonitoredVm(host, this.testApp.getPid());

        this.vm = new JvmstatVm(host, mvm);
    }

    @After
    public void after() throws Exception {
        this.testApp.shutdown();
    }

    @Test
    public void mainClass() {
        assertEquals(TestApp.class.getSimpleName(), this.vm.getMainClass());
    }


    @Test
    public void pid() {
        assertEquals(this.testApp.getPid(), this.vm.getPid());
    }

    @Test
    public void vmArgs() {
        assertEquals(String.format("-D%s=%s", TEST_PROPERTY, TEST_PROPERTY_VALUE), this.vm.getVmArgs());
    }

    @Test
    public void termination() throws Exception {
        Schedulers.timer().schedule(testApp::shutdown, 50, TimeUnit.MILLISECONDS);
        Integer block = this.vm.termination().block(5000);
        assertEquals(testApp.getPid(), block.intValue());

        // check that a second termination signal works
        block = this.vm.termination().block(500);
        assertEquals(testApp.getPid(), block.intValue());
    }

    @Test
    public void sampleMetric() {
        TestSubscriber.subscribe(testSample("java.ci.totalTime"))
                .awaitAndAssertNextValuesWith(t -> {
                    assertTrue(t.t1 < System.currentTimeMillis());
                    assertNotNull(t.t2);
                });
    }

    @Test(expected = IllegalArgumentException.class)
    public void sampleInvalidMetric() {
        testSample("foo");
    }

    @Test
    public void availableMetrics() throws Exception {
        List<String> metricNames = this.vm.availableMetrics().collectList().block();
        assertTrue(metricNames.contains("java.threads.live"));
    }

    @Test
    public void stopSamplingOnDetach() throws Exception {
        Flux<Tuple2<Long, Number>> flux = testSample("sun.os.hrt.ticks", 3000);
        TestSubscriber<Tuple2<Long, Number>> testSubscriber = TestSubscriber.subscribe(flux);

        // kill the app
        this.testApp.shutdown();

        // the test subscriber must be completed
        testSubscriber.await(Duration.of(5, ChronoUnit.SECONDS)).assertComplete();
    }

    private Flux<Tuple2<Long, Number>> testSample(String metricName) {
        return testSample(metricName, 50);
    }

    private Flux<Tuple2<Long, Number>> testSample(String metricName, long millis) {
        return this.vm.sampleMetric(metricName, millis, ChronoUnit.MILLIS);
    }
}
