package com.skipjaq.hotspotmon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * @author Rob Harrop
 */
public class TestApp {

    private static final String STARTUP_BANNER = "STARTED";

    private final Process process;

    private final BufferedReader reader;

    private final int pid;

    public TestApp(Process process) {
        this.process = process;
        this.reader = new BufferedReader(new InputStreamReader(this.process.getInputStream()));
        this.pid = Integer.parseInt(safeReadLine());
    }

    public int getPid() {
        return this.pid;
    }

    private String safeReadLine() {
        try {
            return this.reader.readLine();
        } catch (IOException e) {
            throw new IllegalStateException("Cannot read from spawned process", e);
        }
    }

    public void waitForStart() throws IOException {
        String theLine = this.reader.readLine();
        if (!STARTUP_BANNER.equals(theLine)) {
            throw new IllegalStateException("Spawned task did not start correctly.");
        }
    }

    public void shutdown() {
        if (!this.process.isAlive()) return;

        OutputStream outputStream = this.process.getOutputStream();
        try {
            outputStream.write(new byte[]{'\n'});
            outputStream.flush();
        } catch (IOException e) {
            System.err.println("Unable to shutdown TestApp JVM");
            e.printStackTrace(System.err);
        }
    }

    public static TestApp spawn() throws Exception {
        return spawn(null);
    }

    public static TestApp spawn(Properties systemProperties) throws Exception {
        String javaHome = System.getProperty("java.home");
        String javaBin = Paths.get(javaHome, "bin", "java").toString();
        String classPath = System.getProperty("java.class.path");
        String className = TestApp.class.getName();

        List<String> propertyArgs = Collections.emptyList();
        if (systemProperties != null) {
            propertyArgs = systemProperties.entrySet().stream().map(e -> String.format("-D%s=%s", e.getKey(), e.getValue())).collect(Collectors.toList());
        }

        ProcessBuilder pb = new ProcessBuilder();

        List<String> args = new ArrayList<>(Arrays.asList(javaBin, "-cp", classPath));
        args.addAll(propertyArgs);
        args.add(className);

        pb.command(args);
        Process process = pb.start();
        return new TestApp(process);
    }

    public static void main(String[] args) throws IOException {
        // write out the pid
        System.out.println(TestUtils.getPid());

        // write out the startup marker
        System.out.println(STARTUP_BANNER);

        // flush the banner
        System.out.flush();

        // wait for shutdown
        System.in.read();
    }

    public static int withTestApp(Consumer<TestApp> f) throws Exception {
        return withTestApp(null, f);
    }

    public static int withTestApp(Properties properties, Consumer<TestApp> f) throws Exception {
        TestApp testApp = TestApp.spawn(properties);
        testApp.waitForStart();
        try {
            f.accept(testApp);
        } finally {
            testApp.shutdown();
        }
        return testApp.getPid();
    }
}
