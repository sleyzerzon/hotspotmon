package com.skipjaq.hotspotmon.sampler;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;
import reactor.core.test.TestSubscriber;

import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import static com.skipjaq.hotspotmon.TestApp.withTestApp;
import static com.skipjaq.hotspotmon.sampler.StatementOfInterestPublishers.HOTSPOTMON_PROPERTY_PREFIX;
import static org.junit.Assert.*;

/**
 * @author Rob Harrop
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class ReactiveSamplerIntegrationTests {

    @Autowired
    ReactiveSampler sampler;

    @Test
    public void samplingASingleVm() throws Exception {
        Properties properties = new Properties();
        properties.setProperty(HOTSPOTMON_PROPERTY_PREFIX + "java.ci.totalTime", "100");

        withTestApp(properties, testApp -> {
            // if there are other sampled VMs running in the background, this can fail. The filter prevents that.
            Flux<SampleData> sampler = this.sampler.sampler()
                    .filter(sd -> sd.getPid() == testApp.getPid());

            TestSubscriber.subscribe(sampler).awaitAndAssertNextValuesWith(sd -> {
                assertEquals(testApp.getPid(), sd.getPid());
                assertEquals("java.ci.totalTime", sd.getMetricName());
                assertTrue(sd.getSampleTime() < System.currentTimeMillis());
                assertNotNull(sd.getSampleValue());
            });
        });
    }

    @Test
    public void samplingMultipleMetrics() throws Exception {
        Properties properties = new Properties();
        properties.setProperty(HOTSPOTMON_PROPERTY_PREFIX + "java.ci.totalTime", "100");
        properties.setProperty(HOTSPOTMON_PROPERTY_PREFIX + "sun.ci.totalCompiles", "100");

        withTestApp(properties, testApp -> {
            // if there are other sampled VMs running in the background, this can fail. The filter prevents that.
            Flux<SampleData> sampler = this.sampler.sampler()
                    .filter(sd -> sd.getPid() == testApp.getPid());

            Set<String> metricNames = sampler.map(SampleData::getMetricName).take(10).collect(Collectors.toSet()).block();
            assertTrue(metricNames.contains("java.ci.totalTime"));
            assertTrue(metricNames.contains("sun.ci.totalCompiles"));
        });
    }


}
