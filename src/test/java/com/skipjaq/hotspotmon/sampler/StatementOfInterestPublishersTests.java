package com.skipjaq.hotspotmon.sampler;

import com.skipjaq.hotspotmon.vm.Vm;
import com.skipjaq.hotspotmon.vm.VmWatcher;
import org.junit.Test;
import reactor.core.publisher.Flux;
import reactor.core.test.TestSubscriber;

import java.time.temporal.ChronoUnit;

import static com.skipjaq.hotspotmon.sampler.StatementOfInterestPublishers.HOTSPOTMON_PROPERTY_PREFIX;
import static com.skipjaq.hotspotmon.sampler.StatementOfInterestPublishers.vmArgsPublisher;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Rob Harrop
 */
public class StatementOfInterestPublishersTests {

    @Test
    public void vmArgs() {
        Vm noArg = mock(Vm.class);
        when(noArg.getVmArgs()).thenReturn("");

        Vm arg = mock(Vm.class);
        when(arg.getVmArgs()).thenReturn("-D" + HOTSPOTMON_PROPERTY_PREFIX + "java.ci.totalTime=1000");
        when(arg.getPid()).thenReturn(100);

        VmWatcher vmWatcher = mock(VmWatcher.class);
        when(vmWatcher.watchLocal()).thenReturn(Flux.just(noArg, arg));

        Flux<StatementOfInterest> flux = vmArgsPublisher(vmWatcher);

        TestSubscriber.subscribe(flux).awaitAndAssertNextValuesWith(soi -> {
            assertEquals(100, soi.getPid().intValue());
            assertEquals(1000, soi.getSamplePeriod());
            assertEquals(ChronoUnit.MILLIS, soi.getSampleUnit());
            assertEquals("java.ci.totalTime", soi.getMetricName());
        });

    }


}
