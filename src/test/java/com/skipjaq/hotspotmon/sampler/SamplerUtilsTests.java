package com.skipjaq.hotspotmon.sampler;

import org.junit.Test;

import java.util.Properties;

import static org.junit.Assert.assertEquals;

/**
 * @author Rob Harrop
 */
public class SamplerUtilsTests {

    @Test
    public void singleProperty() {
        Properties p = SamplerUtils.propertiesFromArgString("-Dfoo=bar");
        assertEquals("bar", p.getProperty("foo"));
    }

    @Test
    public void multiProp() {
        Properties p = SamplerUtils.propertiesFromArgString("-Dfoo=bar -Dbaz=boo");
        assertEquals("bar", p.getProperty("foo"));
        assertEquals("boo", p.getProperty("baz"));
    }

    @Test
    public void ignoreOtherArgs() {
        Properties p = SamplerUtils.propertiesFromArgString("-Dfoo=bar -X -Dbar");
        assertEquals("bar", p.getProperty("foo"));
    }

}
