package com.skipjaq.hotspotmon.web;

import com.skipjaq.hotspotmon.vm.Vm;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * @author Rob Harrop
 */
final class Links {

    static ControllerLinkBuilder toVm(Vm vm) {
        return linkTo(methodOn(VmController.class).vm(vm.getPid()));
    }

    static ControllerLinkBuilder toMetrics(Vm vm) {
        return linkTo(methodOn(VmController.class).metrics(vm.getPid()));
    }
}
