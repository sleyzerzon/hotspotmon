package com.skipjaq.hotspotmon.web;

import com.skipjaq.hotspotmon.vm.Vm;
import com.skipjaq.hotspotmon.vm.VmWatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Rob Harrop
 */
@Controller
@RequestMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
public class VmController {

    private final VmWatcher watcher;

    @Autowired
    public VmController(VmWatcher watcher) {
        this.watcher = watcher;
    }

    @RequestMapping()
    @ResponseBody
    public Resources<VmResource> vms() {
        return new Resources<>(this.watcher.activeVms().values().stream().map(VmResource::new).collect(Collectors.toSet()));
    }

    @RequestMapping(path = "/{pid}")
    @ResponseBody
    public VmResource vm(@PathVariable Integer pid) {
        return new VmResource(getVm(pid));
    }

    @RequestMapping(path = "/{pid}/metrics")
    @ResponseBody
    public Resources<MetricResource> metrics(@PathVariable Integer pid) {
        Vm vm = getVm(pid);
        List<MetricResource> metricResources = vm.availableMetrics().map(m -> new MetricResource(vm, m)).collectList().block();
        return new Resources<>(metricResources);
    }

    private Vm getVm(@PathVariable Integer pid) {
        return this.watcher.activeVms().get(pid);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    public static class VmNotFoundException extends RuntimeException {

        public VmNotFoundException(Integer pid) {
            super("Cannot find JVM with PID '" + pid + "'");
        }
    }
}
