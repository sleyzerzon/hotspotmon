package com.skipjaq.hotspotmon.web;

import com.skipjaq.hotspotmon.vm.Vm;
import org.springframework.hateoas.ResourceSupport;

import static com.skipjaq.hotspotmon.web.Links.toMetrics;
import static com.skipjaq.hotspotmon.web.Links.toVm;

/**
 * @author Rob Harrop
 */
class VmResource extends ResourceSupport {

    private final Vm vm;

    public VmResource(Vm vm) {
        this.vm = vm;
        add(toVm(vm).withSelfRel());
        add(toMetrics(vm).withRel("metrics"));
    }

    public int getPid() {
        return vm.getPid();
    }

    public String getMainClass() {
        return vm.getMainClass();
    }
}
