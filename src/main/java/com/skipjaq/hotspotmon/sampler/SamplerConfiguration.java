package com.skipjaq.hotspotmon.sampler;

import com.skipjaq.hotspotmon.vm.VmWatcher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.skipjaq.hotspotmon.sampler.StatementOfInterestPublishers.vmArgsPublisher;

/**
 * {@link Configuration} for the {@link Sampler} subsystem.
 *
 * @author Rob Harrop
 */
@Configuration
public class SamplerConfiguration {

    @Bean
    public Sampler sampler(VmWatcher watcher) {
        return new ReactiveSampler(watcher, vmArgsPublisher(watcher));
    }
}
