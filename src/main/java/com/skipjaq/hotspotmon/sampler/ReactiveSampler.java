package com.skipjaq.hotspotmon.sampler;

import com.skipjaq.hotspotmon.vm.Vm;
import com.skipjaq.hotspotmon.vm.VmWatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.tuple.Tuple2;

/**
 * A {@link Sampler} that multiplexes over many VMs and metrics.
 * <p>
 * The set of sampled VMs and metrics is expressed through {@link StatementOfInterest statements of interest}.
 * These statements are supplied through a {@link Flux} with the intention that they will change as new JVMs
 * are started, and as users dynamically change their interests at runtime.
 *
 * @author Rob Harrop
 */
final class ReactiveSampler implements Sampler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReactiveSampler.class);

    private final VmWatcher watcher;

    private final Flux<StatementOfInterest> soiPublisher;

    /**
     * Creates a new <code>ReactiveSampler</code>.
     *
     * @param watcher the {@link VmWatcher} used to track starting VMs.
     * @param soiPublisher the {@link Flux} from which {@link StatementOfInterest statements of interest} are obtained.
     */
    ReactiveSampler(VmWatcher watcher, Flux<StatementOfInterest> soiPublisher) {
        this.watcher = watcher;
        this.soiPublisher = soiPublisher;
    }

    public Flux<SampleData> sampler() {
        return soiPublisher
                .flatMap(soi -> watcher.watchLocal().filter(vm -> vm.getPid() == soi.getPid()).map(vm -> Tuple2.of(soi, vm)))
                .flatMap(t -> samplesOf(t.t1, t.t2))
                .doOnNext(s -> LOGGER.trace("Sampled: {}", s));
    }

    private Flux<SampleData> samplesOf(StatementOfInterest soi, Vm vm) {
        return vm.sampleMetric(soi.getMetricName(), soi.getSamplePeriod(), soi.getSampleUnit())
                .map(sample -> new SampleData(vm.getPid(), soi.getMetricName(), sample.t1, sample.t2));
    }
}
