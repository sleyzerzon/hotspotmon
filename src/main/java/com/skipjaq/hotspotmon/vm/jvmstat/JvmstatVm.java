package com.skipjaq.hotspotmon.vm.jvmstat;

import com.skipjaq.hotspotmon.Publishers;
import com.skipjaq.hotspotmon.vm.Vm;
import reactor.core.flow.Cancellation;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.core.tuple.Tuple;
import reactor.core.tuple.Tuple2;
import sun.jvmstat.monitor.IntegerMonitor;
import sun.jvmstat.monitor.LongMonitor;
import sun.jvmstat.monitor.Monitor;
import sun.jvmstat.monitor.MonitoredHost;
import sun.jvmstat.monitor.MonitoredVm;
import sun.jvmstat.monitor.MonitoredVmUtil;
import sun.jvmstat.monitor.Variability;

import java.time.temporal.TemporalUnit;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static com.skipjaq.hotspotmon.vm.jvmstat.JvmstatUtils.doOrThrow;

/**
 * @author Rob Harrop
 */
final class JvmstatVm implements Vm {

    private final MonitoredHost host;

    private final MonitoredVm vm;

    JvmstatVm(MonitoredHost host, MonitoredVm vm) {
        this.host = host;
        this.vm = vm;
    }

    public Mono<Integer> termination() {
        return Mono.create(emitter -> {
            Cancellation cancellation = Schedulers.timer().schedulePeriodically(() -> {
                if (!activeVms().contains(getPid())) {
                    emitter.complete(getPid());
                }
            }, 0, 1000L, TimeUnit.MILLISECONDS);
            emitter.setCancellation(cancellation);
        });
    }

    private Set<Integer> activeVms() {
        return doOrThrow(host::activeVms);
    }

    public Flux<String> availableMetrics() {
        List<Monitor> allMonitors = doOrThrow(() -> this.vm.findByPattern(".*"));
        return Flux.fromStream(allMonitors.stream().filter(this::shouldExposeMetric).map(Monitor::getName));
    }


    public Flux<Tuple2<Long, Number>> sampleMetric(String metricName, long interval, TemporalUnit unit) {
        Monitor monitor = doOrThrow(() -> this.vm.findByName(metricName));
        if (monitor == null) {
            throw new IllegalArgumentException("Metric '" + metricName + "' does not exist.");
        }
        if (!shouldExposeMetric(monitor)) {
            throw new IllegalArgumentException("Cannot sample constant metric '" + metricName + "'");
        }
        return Publishers.tick(interval, unit).map(s -> Tuple.of(s, (Number) monitor.getValue())).takeUntil(termination());
    }

    public String getMainClass() {
        return doOrThrow(() -> MonitoredVmUtil.mainClass(this.vm, false));
    }

    public int getPid() {
        return this.vm.getVmIdentifier().getLocalVmId();
    }

    @Override
    public String getVmArgs() {
        return doOrThrow(() -> MonitoredVmUtil.jvmArgs(this.vm));
    }

    private boolean shouldExposeMetric(Monitor monitor) {
        return isMeasurable(monitor) && isNumeric(monitor);
    }

    private boolean isMeasurable(Monitor monitor) {
        return monitor.getVariability() == Variability.MONOTONIC || monitor.getVariability() == Variability.VARIABLE;

    }

    private boolean isNumeric(Monitor monitor) {
        return monitor instanceof LongMonitor || monitor instanceof IntegerMonitor;
    }
}
