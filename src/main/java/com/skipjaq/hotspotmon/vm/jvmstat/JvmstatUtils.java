package com.skipjaq.hotspotmon.vm.jvmstat;

import sun.jvmstat.monitor.MonitorException;
import sun.jvmstat.monitor.MonitoredHost;
import sun.jvmstat.monitor.MonitoredVm;
import sun.jvmstat.monitor.VmIdentifier;

import java.net.URISyntaxException;

/**
 * @author Rob Harrop
 */
final class JvmstatUtils {

    static MonitoredHost getLocalMonitoredHost() {
        try {
            return MonitoredHost.getMonitoredHost("//localhost");
        } catch (MonitorException | URISyntaxException e) {
            throw new IllegalStateException("Cannot access hotspot on localhost.", e);
        }
    }

    static MonitoredVm getMonitoredVm(MonitoredHost host, Integer id) {
        try {
            return host.getMonitoredVm(createVmIdentifier(id));
        } catch (MonitorException e) {
            throw new IllegalStateException("Cannot access local VM with ID: " + id);
        }
    }

    private static VmIdentifier createVmIdentifier(Integer id) {
        String vmid = "//" + id + "?mode=r";
        try {
            return new VmIdentifier(vmid);
        } catch (URISyntaxException e) {
            throw new IllegalStateException("Invalid VMID '" + vmid + "'", e);
        }
    }

    static void doOrThrow(JvmstatRunnable r) {
        doOrThrow(() -> {
            r.run();
            return null;
        });
    }

    static <T> T doOrThrow(JvmstatCallable<T> f) {
        try {
            return f.call();
        } catch (MonitorException ex) {
            // TODO: log
            // TODO: wrap with a decent type
            throw new RuntimeException(ex);
        }
    }

    @FunctionalInterface
    interface JvmstatCallable<T> {

        T call() throws MonitorException;
    }

    @FunctionalInterface
    interface JvmstatRunnable {

        void run() throws MonitorException;
    }
}
