package com.skipjaq.hotspotmon.vm.jvmstat;

import com.skipjaq.hotspotmon.vm.Vm;
import com.skipjaq.hotspotmon.vm.VmWatcher;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import sun.jvmstat.monitor.MonitorException;
import sun.jvmstat.monitor.MonitoredHost;
import sun.jvmstat.monitor.event.HostEvent;
import sun.jvmstat.monitor.event.HostListener;
import sun.jvmstat.monitor.event.VmStatusChangeEvent;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static com.skipjaq.hotspotmon.vm.jvmstat.JvmstatUtils.doOrThrow;

/**
 * @author Rob Harrop
 */
@Component
public class JvmstatVmWatcher implements VmWatcher {

    private final ConcurrentMap<Integer, Vm> activeVms = new ConcurrentHashMap<>();

    public JvmstatVmWatcher() {
        watchLocal().subscribe(vm -> {
            this.activeVms.putIfAbsent(vm.getPid(), vm);
            vm.termination().subscribe(this.activeVms::remove);
        });
    }

    public Flux<Vm> watchLocal() {
        return watchHost(JvmstatUtils.getLocalMonitoredHost());
    }

    @Override
    public ConcurrentMap<Integer, Vm> activeVms() {
        return this.activeVms;
    }


    Flux<Vm> watchHost(MonitoredHost monitoredHost) {
        return Flux.create(emitter -> {
            // We need a listener for VM events that will push events out from the publisher
            HostListener hostListener = new HostListener() {
                @Override
                @SuppressWarnings("unchecked")
                public void vmStatusChanged(VmStatusChangeEvent vmStatusChangeEvent) {
                    emitEvents(vmStatusChangeEvent.getStarted());
                }

                @Override
                public void disconnected(HostEvent hostEvent) {
                    emitter.complete();
                }

                private void emitEvents(Set<Integer> vms) {
                    vms.stream().forEach(vm -> {
                        if (!emitter.isCancelled()) {
                            emitter.next(new JvmstatVm(monitoredHost, JvmstatUtils.getMonitoredVm(monitoredHost, vm)));
                        }
                    });
                }
            };

            // Add the listener for the emitter
            try {
                monitoredHost.addHostListener(hostListener);
            } catch (MonitorException e) {
                emitter.fail(e);
            }

            // When cancelling, make sure to remove the listener
            emitter.setCancellation(() -> doOrThrow(() -> monitoredHost.removeHostListener(hostListener)));
        });
    }
}
