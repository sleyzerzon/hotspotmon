package com.skipjaq.hotspotmon.vm;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.tuple.Tuple2;

import java.time.temporal.TemporalUnit;

/**
 * A Java Virtual Machine that exposes metric to be sampled.
 *
 * @author Rob Harrop
 */
public interface Vm {

    /**
     * Gets the PID of this JVM.
     *
     * @return the PID.
     */
    int getPid();

    /**
     * Gets the name of the main class for the Java application running in this JVM.
     *
     * @return the name of the main class
     */
    String getMainClass();

    /**
     * Gets the arguments supplied to the JVM.
     * <p>
     * Note that these are <strong>not</strong> the arguments supplied to the Java
     * application itself.
     *
     * @return the JVM arguments.
     */
    String getVmArgs();

    /**
     * Creates a {@link Mono} that emits the PID of this JVM when the JVM terminates.
     *
     * @return the termination <code>Mono</code>.
     */
    Mono<Integer> termination();

    /**
     * Creates a {@link Flux} that emits the names of all available metrics for this JVM.
     *
     * @return the metric names
     */
    Flux<String> availableMetrics();

    /**
     * Returns a {@link Flux} to sample a given metric at a given interval.
     *
     * @param metricName the name of the metric to sample
     * @param interval   the interval at which to sample
     * @param unit       the {@link TemporalUnit} of the sample interval
     * @return the sampling <code>Flux</code>
     */
    Flux<Tuple2<Long, Number>> sampleMetric(String metricName, long interval, TemporalUnit unit);

}
