A simple agent for monitoring internal counters from the Hotspot JVM.

## Building

Hotspotmon uses Gradle for build. To build this project on your machine simply
run:

```
./gradlew bootRepackage
```

This will build the Hotspotmon JAR and place it in `build/libs`.

To run the tests:

```
./gradlew build
```

For the full test suite, you'll need to have InfluxDB running on the localhost, 
with the HTTP endpoint running on port `8086`. By default, the test use
`root/root` as the credentials to connect to InfluxDB, and will connect to a
database called `hotspotmon`.

## Running

Hotspotmon is packaged as an executable JAR file. To run it:

```
java -jar <path_to_hotspotmon_jar>
```

By default, the web frontend runs on port `8900`, and the InfluxDB
settings are the same as those used for testing.

### Configuring

Hotspotmon uses standard Spring Boot-style configuration. For the full
list of configuration options see the source code. A summary of the most
useful options is given here.

#### Changing the HTTP Port

To change the HTTP port, start the application with `--server.port`:

```
java -jar hotspotmon.jar --server.port=8080
```

#### Configuring the InfluxDB Connection

The following command-line flags are provided to control the InfluxDB 
connection behaviour:

|Flag                   |Purpose                                                             |Default              |
|-----------------------|--------------------------------------------------------------------|---------------------|
|`influxdb.url`         |The HTTP url of the InfluxDB server                                 |http://localhost:8086|
|`influxdb.user`        |The username used to connect to InfluxDB                            |`root`               |
|`influxdb.password`    |The password used to connect to InfluxDB                            |`root`               |
|`influxdb.database`    |The InfluxDB database where sample data is to be stored             |`hotspotmon`         |
|`influxdb.batchSize`   |The maximum size of each batch of sample data                       |`100`                |
|`influxdb.batchSeconds`|The cut-off in seconds to wait before sending a batch of sample data|`1`                  |

## Monitoring Hotspot JVMs

To monitor a Hotspot VM using hotspotmon, it is enough to start that VM with system properties of the format
`-Dcom.skipjaq.hotspotmon.<metric_name>=<sample_millis>`. For example, to sample the `sun.ci.standardCompiles`
and `sun.ci.osrCompiles` metrics every five seconds:

```
 java -Dcom.skipjaq.hotspotmon.sun.ci.standardCompiles=5000 -Dcom.skipjaq.hotspotmon.sun.ci.osrCompiles=5000 -jar myapp.jar
```
